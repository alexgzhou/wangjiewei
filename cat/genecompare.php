<?php
 
$seqdir = dirname(__FILE__);
require_once($seqdir.'\\phpexcel\\Classes\\PHPExcel.php');
$seqdirs = scandir($seqdir);
// var_dump($seqdirs);
// print_r ($seqdirs);

$objPHPExcel = new PHPExcel();
$filePath = 'E3-CRISPR.xlsx';
/**默认用excel2007读取excel，若格式不对，则用之前的版本进行读取*/ 
$PHPReader = new PHPExcel_Reader_Excel2007(); 
if(!$PHPReader->canRead($filePath)){ 
    $PHPReader = new PHPExcel_Reader_Excel5(); 
    if(!$PHPReader->canRead($filePath)){ 
        echo 'no Excel'; 
        return ; 
    } 
}

$PHPExcel = $PHPReader->load($filePath); 
/**读取excel文件中的第三个工作表*/ 
$currentSheet = $PHPExcel->getSheet(2); 
/**取得最大的列号*/ 
$allColumn = $currentSheet->getHighestColumn(); 
/**取得一共有多少行*/ 
$allRow = $currentSheet->getHighestRow(); 
/**从第二行开始输出，因为excel表中第一行为列名*/ 
$gene = [];
for($currentRow = 2;$currentRow <= $allRow;$currentRow++){ 
    /**从第A列开始输出*/ 
    $geneSeq = $currentSheet->getCellByColumnAndRow(ord("C") - 65,$currentRow)->getValue();/**ord()将字符转为十进制数*/ 
    $geneSym = $currentSheet->getCellByColumnAndRow(ord("A") - 65,$currentRow)->getValue();/**ord()将字符转为十进制数*/ 
    $geneName = $currentSheet->getCellByColumnAndRow(ord("B") - 65,$currentRow)->getValue();/**ord()将字符转为十进制数*/ 

    $geneSeq = trim($geneSeq);
    $geneSym = trim($geneSym);
    $geneName = trim($geneName);

    try {
        if ($geneSeq) {
            $gene[$geneSeq]['geneSym'] = $geneSym ? $geneSym : "";
            $gene[$geneSeq]['geneName'] = $geneName ? $geneName : "";
            $gene[$geneSeq]['count'] = 0;
            $gene[$geneSeq]['geneSeq'] = $geneSeq ? $geneSeq : "";
            $gene[$geneSeq]['geneMatch'] = "";
            $gene[$geneSeq]['geneCat'] = "";
        } 
    }
    catch (Exception $e) {
        echo $geneSym."-----".$e;
    }
} 

// $outputfile = fopen("test.txt", "a+");
// fputs($outputfile, var_export($gene, TRUE));
// fclose ($outputfile);

foreach ($seqdirs as $subdir) {
    
    if ($subdir !== '.' && $subdir !== '..' && $subdir !== 'phpexcel' && !strpos($subdir,".")) {
        // echo "\n\r".$subdir."\n\r";

        $seqfiles = scandir($subdir);
        // print_r ($seqfiles);

        foreach ($seqfiles as $seqfile) {
            if ($seqfile !== '.' && $seqfile !== '..') {
                // echo "\n\r".$seqdir."\\".$subdir."\\".$seqfile."\n\r";

                $filehandle = fopen($seqdir."\\".$subdir."\\".$seqfile, "r");

                while (!feof($filehandle)) {
                    $line = trim(fgets($filehandle));
                    if ($line) {
                        // echo $line."\n";
                        foreach ($gene as &$array) {
                            // echo $array['geneSym'] ? $array['geneSym']."\n" : "";
                            $isMatched = preg_match_all("/$line/", $array['geneSeq'], $matches);
                            if ($isMatched) {
                                // echo $array['geneSym'] ? $array['geneSym']."\n" : "";
                                foreach ($matches as $val) {
                                    if ($val) {
                                        // echo $val[0]."\n";
                                        $array['count']++;
                                        // echo $array['geneSeq'] ? $array['geneSeq']."==" : "";
                                        // echo $array['count']."\n";
                                        $array['geneMatch'] = $array['geneMatch'] ? $array['geneMatch']."\r\n".$seqfile : $seqfile;
                                        $array['geneCat'] = $array['geneCat'] ? $array['geneCat']."\r\n".$subdir : $subdir;
                                    }
                                }
                            }
                        }
                        unset($array); // 最后取消掉引用
                    }
                }
                fclose ($filehandle);
            }
        }
    }
}

// var_dump($gene);

foreach ($gene as $array) {
    if ($array['count'] > 0) {
        // echo $array['count']."#####".$array['geneSym']."#####".$array['geneName']."#####".$array['geneSeq']."#####".$array['geneMatch']."\n";
        // echo $array['count'];
        // $result = "(匹配次数:".$array['count']."); (符号:".$array['geneSym']."); (名称:".$array['geneName']."); (序列:".$array['geneSeq']."); (样本:".$array['geneMatch']."); (分类:".$array['geneCat'].").\r\n";

        //替换掉其中的逗号","
        $array['geneSym'] = preg_replace("/,/", ';', $array['geneSym']);
        $array['geneName'] = preg_replace("/,/", ';', $array['geneName']);
        //在写CSV文件时, 如果要让一个单元格内的内容换行的话, 需要将这个单元格的内容都用双引号括起来, 如"aaabbb", 然后在需要换行的位置写入换行的转义字符, 变成"aaa\r\nbbb"的形式; 在php中就要用引号"\""将变量包裹起来, 但是在变量内部有\r\n, 如: $var = $array['geneCat']."\r\n".$subdir
        $result = "\"".$array['count']."\",\"".$array['geneSym']."\",\"".$array['geneName']."\",\"".$array['geneSeq']."\",\"".$array['geneCat']."\",\"".$array['geneMatch']."\"\r\n";
        $outputfile = fopen($seqdir."\\result.txt", "a+");
        fputs($outputfile, $result);
        fclose ($outputfile);
    }
}